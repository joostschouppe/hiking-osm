# hiking osm

A web site for hiking.osm.be

## Structure

* html: folder for the website
* frames: folder for the frames, i.e., a gpkg layer for positionning the maps

### frames

The QGIS project at frames/templates.qgz show the frame.gpkg layer that shows all the maps that have been produced so far.

To convert this frame.gpkg layer into a the layer that is hosted on the cartofixer webmap, use the "delete holes" QGIS tool.

This frame layer is intended to be the only source of info about which maps was produced and where.

## Other resources

### Producing maps

The cartoCSS and mapnik code for producing the OpenArdenneMap are living in their own repo at https://github.com/nobohan/OpenArdenneMap. For producing the maps, use the "templates" as indicated in https://github.com/nobohan/OpenArdenneMap/tree/master/templates/README.md.

### The webmap

A webmap using [cartofixer](https://cartofixer.be) is displayed in the website. This webmap uses the frame.gpkg layer in the `html/frames` folder.

The ressource for the web map frame layer is the `.geojson` file stored at `html/frames/frame-full-4326.geojson`.
**important :** The geojson file must be in `WGS 84/EPSG:4326`, and each feature should have an `id`.

This layer is prepared from the `html/frames/frame.gpkg layer` using the "delete holes" QGIS tool. For exporting the data as a standard geojson file, choose EPSG:4326 as CRS and write `id_field=fid` in the Layer tab of the custom options (in the Save as dialgo box of QGIS).


### Thumbnails

Maps thumbnails are stored at `html/frames/thumbnails`. Their url can be referenced as a feature attribute in the `geojson` file, so they can be used as map thumbnails when clicking on a feature on the webmap.

### Storing the maps

Maps are stored in the Champs-Libres nextcloud instance at `Champs-Libres/OpenArdenneMap/cartes/`.

Downloadable maps should be at a decent but reasonnable resolution, maybe 300 dpi.